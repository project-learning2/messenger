import { faCircleXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function AuthFail() {
  const navigate = useNavigate();

  return (
    <div style={{ textAlign: "center" }}>
      <h3>Xác thực thất bại</h3>
      <FontAwesomeIcon
        style={{ color: "#dc3545", fontSize: "2.5rem" }}
        icon={faCircleXmark}
      />
      <p>Có gì đó không tốt xảy ra, hãy liên hệ với tôi</p>
      <Button
        variant="danger"
        onClick={() => {
          navigate("/");
        }}
      >
        Trở lại trang chủ
      </Button>
    </div>
  );
}
