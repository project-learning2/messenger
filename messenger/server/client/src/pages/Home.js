import { Row, Col, Button } from "react-bootstrap";
import { useState } from "react";

import styles from "../styles/Home.module.css";
import Login from "../components/Login/Login";
import Register from "../components/Register/Register";

export default function Home() {
  const [contentDisplay, setContentDisplay] = useState(
    <img
      style={{ width: "100%" }}
      src={process.env.REACT_APP_DOMAIN + "banner.png"}
      alt="banner"
    />
  );

  const clickLoginBtn = () => {
    const loginBtn = document.getElementById("login-btn");
    if (!loginBtn.classList.contains("hide")) {
      loginBtn.classList.add("hide");
    }
    setContentDisplay(<Login changeStateHandle={clickRegister} />);
  };
  const clickRegister = () => {
    setContentDisplay(<Register changeStateHandle={clickLoginBtn} />);
  };

  return (
    <>
      <Row className={`${styles.home}`}>
        <Col
          md={6}
          className={`d-flex align-items-center flex-column justify-content-center ${styles.banner}`}
        >
          <div>
            <h1>
              Kết nối
              <br />
              với mọi người
              <br />
              sâu sắc hơn
            </h1>
            <p className="">
              Messenger có tất cả mọi thứ bạn cần để cảm thấy gần gũi hơn với
              những người thân yêu.
            </p>
            <Button id="login-btn" type="button" onClick={clickLoginBtn}>
              Đăng nhập ngay
            </Button>
          </div>
        </Col>
        <Col
          md={6}
          className="d-flex align-items-center justify-content-center"
        >
          {contentDisplay}
        </Col>
      </Row>
    </>
  );
}
