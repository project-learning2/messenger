import { faCircleCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function AuthSucess() {
  const navigate = useNavigate();

  return (
    <div style={{ textAlign: "center" }}>
      <h3>Xác thực thành công</h3>
      <FontAwesomeIcon
        style={{ color: "#198754", fontSize: "2.5rem" }}
        icon={faCircleCheck}
      />
      <p>Tài khoản của bạn đã được khởi tạo</p>
      <Button
        variant="success"
        onClick={() => {
          navigate("/");
        }}
      >
        Tiếp tục
      </Button>
    </div>
  );
}
