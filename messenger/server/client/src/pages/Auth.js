import axios from "axios";
import { useParams } from "react-router-dom";
import Loading from "./Loading";
import { useEffect, useState, useCallback } from "react";
import AuthSucess from "./AuthSucess";
import AuthFail from "./AuthFail";

export default function Auth() {
  const token = useParams().token;
  const [loading, setLoading] = useState(true);
  const [displayContent, setDisplayContent] = useState(<AuthFail />);

  const handleAPI = useCallback(() => {
    axios
      .get(process.env.REACT_APP_URL_API + "/auth-email/" + token)
      .then((response) => {
        setLoading(false);
        setDisplayContent(<AuthSucess />);
      })
      .catch((err) => {
        setLoading(false);
      });
  }, [token]);

  useEffect(() => {
    handleAPI();
  }, [handleAPI]);

  return loading ? <Loading /> : displayContent;
}
