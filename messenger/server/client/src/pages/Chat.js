import { Col, Row, Tooltip, OverlayTrigger, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCircle,
  faCirclePlus,
  faGear,
  faImage,
  faMagnifyingGlass,
  faMicrophone,
  faNoteSticky,
  faPaperPlane,
  faRightFromBracket,
  // faPaperclip,
} from "@fortawesome/free-solid-svg-icons";
import { useCallback, useEffect, useRef, useState } from "react";
import openSocket from "socket.io-client";
import { useSelector, useDispatch } from "react-redux";
import Spinner from "react-bootstrap/Spinner";

import styles from "../styles/Chat.module.css";
import axios from "axios";
import SettingModal from "../components/Modal/Setting";
import { authActions } from "../store/auth";
import { useNavigate } from "react-router-dom";

export default function Chat() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const socket = useRef(null);
  const [focusSearch, setFocusSearch] = useState(false);
  const [enteredSearch, setEnteredSearch] = useState("");
  const [userOnline, setUserOnline] = useState(null);
  const [contactNow, setContactNow] = useState(null);
  const [enteredMess, setEnteredMess] = useState("");
  const [dataMess, setDataMess] = useState(null);
  const [messRender, setMessRender] = useState(null);
  const [content, setContent] = useState(null);
  const userLogin = useSelector((state) => state.auth.userLogin);
  const [relatedChat, setRelatedChat] = useState(null);
  const [settingModalState, setSettingModalState] = useState(false);

  const fetchRelatedChat = useCallback(() => {
    axios
      .get(process.env.REACT_APP_URL_API + "/get-related-chat/" + userLogin._id)
      .then((response) => {
        setRelatedChat(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [userLogin]);
  // Handle socket.io
  useEffect(() => {
    if (!userLogin) {
      window.location.href = "/";
    } else {
      socket.current = openSocket(process.env.REACT_APP_URL_API.slice(0, -3));
      // user online send event to server
      socket.current.on("connect", () => {
        socket.current.emit("user-online", userLogin);
      });
      socket.current.on("newUserOnline", (userOnlineArr) => {
        const userIndex = userOnlineArr.findIndex(
          (el) => el._id === userLogin._id
        );
        if (userIndex !== -1) {
          userOnlineArr.splice(userIndex, 1);
          setUserOnline(userOnlineArr);
        }
      });
      socket.current.on("messReceived", (data) => {
        if (contactNow) {
          setMessRender((pre) => [...pre, data.contentMess]);
        }
        // fetchRelatedChat();
      });
      fetchRelatedChat();
      return () => {
        socket.current.disconnect();
      };
    }
  }, [userLogin, fetchRelatedChat, contactNow]);

  // change Input
  const changeSearchHandle = (e) => {
    setEnteredSearch(e.target.value);
    if (e.target.value !== "") {
      setFocusSearch(false);
    } else {
      setFocusSearch(true);
    }
  };

  const userOnlineBlock =
    userOnline && userOnline.length !== 0 ? (
      userOnline.map((el) => {
        return (
          <OverlayTrigger
            placement="right"
            delay={{ show: 250, hide: 400 }}
            key={el._id}
            overlay={(props) => {
              return (
                <Tooltip id="button-tooltip" {...props}>
                  {el.fullName}
                </Tooltip>
              );
            }}
          >
            <div
              className={`${styles["userOnlineImg"]} hoverr rounded-circle d-flex align-items-center justify-content-center`}
              style={{ transform: "scale(1.2)" }}
              onClick={() => clickUserHandle(el)}
            >
              <img alt="avatar" src={el.avatar_img} />
            </div>
          </OverlayTrigger>
        );
      })
    ) : (
      <p>0 user online</p>
    );

  const startChatHandle = (user) => {
    axios
      .post(process.env.REACT_APP_URL_API + "/create-chat-room", {
        users: [user._id, userLogin._id],
      })
      .then((response) => {
        if (response.status === 201) {
          setDataMess(response.data.chatRoom);
          const startChatBtn = document.getElementById("start-chat-btn");
          startChatBtn.classList.add("hide");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const sendMessHandle = useRef(() => {
    if (enteredMess !== "") {
      socket.current.emit("onChat", {
        chatRoom: dataMess._id,
        toUser: contactNow._id,
        contentMess: {
          user: userLogin._id,
          message: enteredMess,
          date: new Date(),
        },
      });
      setEnteredMess("");
    }
  });
  useEffect(() => {
    sendMessHandle.current = () => {
      if (enteredMess !== "") {
        setMessRender((preMessRender) => [
          ...preMessRender,
          {
            user: userLogin._id,
            message: enteredMess,
            date: new Date(),
          },
        ]);
        fetchRelatedChat();
        socket.current.emit("onChat", {
          chatRoom: dataMess._id,
          toUser: contactNow._id,
          contentMess: {
            user: userLogin._id,
            message: enteredMess,
            date: new Date(),
          },
        });
        setEnteredMess("");
      }
    };
  }, [contactNow, enteredMess, userLogin, dataMess, fetchRelatedChat]);

  function clickUserHandle(user) {
    setContactNow(user);
    setContent("Loading");
    axios
      .get(
        process.env.REACT_APP_URL_API +
          "/chat/" +
          `${user._id}/${userLogin._id}`
      )
      .then((response) => {
        if (response.data) {
          setDataMess(response.data);
          setMessRender(response.data.contentMessage);
          setContent("exitsMess");
        } else {
          setContent("unExitsMess");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  const handleLogout = () => {
    axios
      .post(process.env.REACT_APP_URL_API + "/logout")
      .then((response) => {
        if (response.status === 200) {
          dispatch(authActions.logout());
          navigate("/");
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <>
      <SettingModal
        show={settingModalState}
        handleClose={() => {
          setSettingModalState(false);
        }}
        handleShow={() => {
          setSettingModalState(true);
        }}
      />
      <Row className={`${styles.chat} px-3`}>
        <Col md={2} className={`${styles["chat-list"]} p-0`}>
          <div className="d-flex align-items-center justify-content-between px-3 py-2">
            <h3 className="m-0">Chats</h3>
            <div className="d-flex">
              <div
                className={`${styles["icon-wrapper"]} hoverr rounded-circle d-flex align-items-center justify-content-center`}
                style={{ transform: "scale(1.2)" }}
                onClick={() => setSettingModalState(true)}
              >
                <FontAwesomeIcon icon={faGear} />
              </div>
              <div
                className={`${styles["icon-wrapper"]} hoverr rounded-circle d-flex align-items-center justify-content-center`}
                style={{ transform: "scale(1.2)" }}
                onClick={handleLogout}
              >
                <FontAwesomeIcon icon={faRightFromBracket} />
              </div>
            </div>
          </div>
          <div className={`px-3 ${styles["search-input"]} position-relative`}>
            {focusSearch && (
              <p
                style={{
                  color: "#767676",
                  position: "absolute",
                  top: "6px",
                  left: "20px",
                }}
              >
                <span>
                  <FontAwesomeIcon className="px-2" icon={faMagnifyingGlass} />
                </span>
                Search Messenger
              </p>
            )}
            <input
              type="text"
              placeholder={focusSearch ? "" : "Search Messenger"}
              onBlur={() => {
                setFocusSearch(false);
              }}
              onFocus={() => {
                if (enteredSearch === "") {
                  setFocusSearch(true);
                }
              }}
              onChange={changeSearchHandle}
              value={enteredSearch}
            />
          </div>
          <div className={styles.t} style={{ overflowX: "auto" }}>
            <div
              className={`px-3 ${styles["user-online-display"]} d-flex gap-3 mt-3 align-content-center`}
            >
              {userOnlineBlock}
            </div>
          </div>
          <div className={`${styles.relatedChat}`}>
            {relatedChat &&
              relatedChat.map((el) => {
                const toUserIndex = el.users.findIndex(
                  (user) => user._id !== userLogin._id
                );
                return (
                  <div
                    className={`${styles["chatRoomTag"]} p-2 hoverr`}
                    key={el._id}
                    onClick={() => clickUserHandle(el.users[toUserIndex])}
                  >
                    <img
                      src={el.users[toUserIndex].avatar_img}
                      alt="ava"
                      width={56}
                    />
                    <div>
                      <h5>{el.users[toUserIndex].fullName}</h5>
                    </div>
                  </div>
                );
              })}
          </div>
        </Col>
        <Col md={8} className={`${styles["content"]} p-0 d-flex flex-column`}>
          <div
            className={`${styles["address-to"]} d-flex align-items-center px-3`}
            md={1}
          >
            {contactNow && (
              <div className="d-flex">
                <div
                  className={`${styles["userOnlineImg"]} hoverr rounded-circle d-flex align-items-center justify-content-center`}
                >
                  <img alt="avatar" src={contactNow.avatar_img} />
                </div>
                <div style={{ lineHeight: "1.2" }}>
                  <h5 className="m-0 d-flex align-items-center mx-3">
                    {contactNow.fullName}
                  </h5>
                  <p className="m-0 mx-3" style={{ fontSize: "0.75rem" }}>
                    {userOnline.find((el) => el._id === contactNow._id) ? (
                      <span>
                        <FontAwesomeIcon
                          style={{
                            color: "green",
                            fontSize: "0.75rem",
                            marginRight: "0.25rem",
                          }}
                          icon={faCircle}
                        />
                        Online
                      </span>
                    ) : (
                      "Offline"
                    )}
                  </p>
                </div>
              </div>
            )}
          </div>
          <div className={`${styles["message-content"]} position-relative`}>
            {content === "Loading" && (
              <div
                style={{
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  transform: "translate(-50%,-50%)",
                }}
              >
                <Spinner animation="border" role="status">
                  <span className="visually-hidden">Loading...</span>
                </Spinner>
              </div>
            )}
            {content === "exitsMess" && (
              <div className={styles["mess-content"]}>
                {messRender &&
                  messRender.map((el) => {
                    return (
                      <div
                        key={el.date}
                        className={
                          el.user === userLogin._id
                            ? `${styles["send-mess"]}`
                            : `${styles["received-mess"]}`
                        }
                      >
                        <div className={styles["mess"]}>{el.message}</div>
                      </div>
                    );
                  })}
              </div>
            )}
            {content === "unExitsMess" && (
              <Button
                type="button"
                onClick={() => startChatHandle(contactNow)}
                className="position-absolute"
                id="start-chat-btn"
                style={{
                  bottom: "2rem",
                  left: "50%",
                  transform: "translate(-50%,0)",
                }}
              >
                Start chat
              </Button>
            )}
          </div>
          {contactNow && (
            <div
              className={`${styles["message-input"]} d-flex align-items-center`}
              md={1}
            >
              <div
                className="d-flex gap-2 justify-content-center px-3"
                style={{ width: "18%" }}
              >
                <div
                  className={`${styles["icon-wrapper"]} hoverr rounded-circle d-flex align-items-center justify-content-center`}
                >
                  <FontAwesomeIcon icon={faCirclePlus} />
                </div>
                <div
                  className={`${styles["icon-wrapper"]} hoverr rounded-circle d-flex align-items-center justify-content-center`}
                >
                  <FontAwesomeIcon icon={faMicrophone} />
                </div>
                <div
                  className={`${styles["icon-wrapper"]} hoverr rounded-circle d-flex align-items-center justify-content-center`}
                >
                  <FontAwesomeIcon icon={faImage} />
                </div>
                <div
                  className={`${styles["icon-wrapper"]} hoverr rounded-circle d-flex align-items-center justify-content-center`}
                >
                  <FontAwesomeIcon icon={faNoteSticky} />
                </div>
              </div>
              <div className={`${styles["input-wrapper"]}`}>
                <form
                  onSubmit={(e) => {
                    e.preventDefault();
                    sendMessHandle.current();
                  }}
                >
                  <textarea
                    type="text"
                    placeholder="Aaa"
                    value={enteredMess}
                    onChange={(e) => {
                      setEnteredMess(e.target.value);
                    }}
                    onKeyDown={(e) => {
                      if (e.key === "Enter" && enteredMess !== "") {
                        sendMessHandle.current();
                      }
                    }}
                  />
                </form>
              </div>
              <div
                className={`${styles["send-btn"]} d-flex justify-content-center`}
                onClick={() => sendMessHandle.current()}
              >
                <div
                  className={`${styles["icon-wrapper"]} hoverr m-0 rounded-circle d-flex align-items-center justify-content-center`}
                >
                  <FontAwesomeIcon icon={faPaperPlane} />
                </div>
              </div>
            </div>
          )}
        </Col>
        <Col md={2} className={`${styles["Infor"]}`}>
          More Infor
        </Col>
      </Row>
    </>
  );
}
