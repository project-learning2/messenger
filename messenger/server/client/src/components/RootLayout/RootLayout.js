import { Container } from "react-bootstrap";
import { Outlet, useNavigate } from "react-router-dom";
import { Row, Col } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import axios from "axios";

import MainNav from "../MainNav/MainNav";
import { authActions } from "../../store/auth";
import Loading from "../../pages/Loading";

export default function RootLayout() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [loading, setLoading] = useState(true);
  // console.log(process.env.REACT_APP_URL_API);

  useEffect(() => {
    setTimeout(() => {
      axios
        .get(process.env.REACT_APP_URL_API + "/get-session")
        .then((response) => {
          dispatch(authActions.login(response.data));
          navigate(`/messenger/${response.data.email.split("@")[0]}`);
          setLoading(false);
        })
        .catch((err) => {
          // console.log(err);
          if (err.response.status === 404) {
            console.log("No session exits");
            setLoading(false);
          } else {
            console.log("Something went wrong!!");
          }
        });
    }, 1000);
  }, [dispatch, navigate]);

  return (
    <>
      {!loading ? (
        <Container fluid="lg">
          <MainNav />
          <main style={{ marginTop: "100px" }}>
            <Outlet />
          </main>
          <footer>
            <Row>
              <Col md={1} className="p-0 d-flex align-items-center">
                <span>
                  <strong>© Meta 2023.</strong>
                </span>
              </Col>
              <Col md={"auto"} className="d-flex align-items-center">
                <span>
                  Logo của Meta và Google Play là nhãn hiệu hàng hóa thuộc chủ
                  sở hữu tương ứng.
                </span>
              </Col>
              <Col className="p-0 d-flex align-items-center">
                <Row>
                  <Col className="d-flex align-items-center">
                    <span>Chính sách quyền riêng tư</span>
                  </Col>
                  <Col md={"auto"} className="d-flex align-items-center">
                    <span>Chính sách cookie</span>
                  </Col>
                  <Col md={"auto"} className="d-flex align-items-center">
                    <span>Điều khoản</span>
                  </Col>
                  <Col md={"auto"} className="p-0">
                    <img
                      width={150}
                      src="https://scontent.fsgn5-11.fna.fbcdn.net/v/t39.8562-6/284131241_398860802255675_7090232386370085328_n.png?_nc_cat=1&ccb=1-7&_nc_sid=6825c5&_nc_ohc=L05SZoFTsZIAX-qAXYe&_nc_ht=scontent.fsgn5-11.fna&oh=00_AfAHn533hSG0ZFUOL5RmpR7jN4jwoD5i00PM9zeXQEhIlw&oe=64496446"
                      alt="test"
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
          </footer>
        </Container>
      ) : (
        <Loading />
      )}
    </>
  );
}
