import { useState } from "react";
import { Row, Col, Button, Modal } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCircleCheck,
  faCircleExclamation,
} from "@fortawesome/free-solid-svg-icons";

import styles from "../../styles/auth.module.css";

export default function Register({ changeStateHandle }) {
  const [enteredEmail, setEnteredEmail] = useState("");
  const [enteredFullName, setEnteredFullName] = useState("");
  const [enteredPassword, setEnteredPassword] = useState("");
  const [showOverplayEmail, setShowOverplayEmail] = useState(false);
  const [showOverplayFullName, setShowOverplayFullName] = useState(false);
  const [showOverplayPassword, setShowOverplayPassword] = useState(false);
  const [registerSuccessModal, setRegisterSuccessModal] = useState(false);

  const changeEmailHandle = (e) => {
    setEnteredEmail(e.target.value);
  };
  const changeFullNameHandle = (e) => {
    setEnteredFullName(e.target.value);
  };
  const changePasswordHandle = (e) => {
    setEnteredPassword(e.target.value);
  };
  function isEmail(value) {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(value);
  }
  const outFocusEmail = (e) => {
    if (!isEmail(enteredEmail)) {
      setShowOverplayEmail(true);
    } else {
      setShowOverplayEmail(false);
    }
  };
  const outFocusFullName = () => {
    if (enteredFullName.trim().length === 0) {
      setShowOverplayFullName(true);
    } else {
      setShowOverplayFullName(false);
    }
  };
  const outFocusPassword = () => {
    if (enteredPassword.trim().length <= 8) {
      setShowOverplayPassword(true);
    } else {
      setShowOverplayPassword(false);
    }
  };

  const submitHandle = (e) => {
    e.preventDefault();
    if (
      !(
        !isEmail(enteredEmail) ||
        enteredPassword.trim().length <= 8 ||
        enteredFullName.trim().length === 0
      )
    ) {
      const formData = {
        email: enteredEmail,
        fullName: enteredFullName,
        password: enteredPassword,
      };
      console.log(formData);
      axios
        .post(`${process.env.REACT_APP_URL_API}/create-user`, formData)
        .then((response) => {
          console.log(response);
          if (response.status === 200) {
            setRegisterSuccessModal(true);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  return (
    <>
      <Modal
        show={registerSuccessModal}
        onHide={() => setRegisterSuccessModal(false)}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Kiểm tra email của bạn để xác thực</Modal.Title>
        </Modal.Header>
        <Modal.Body className="d-flex justify-content-center">
          <FontAwesomeIcon
            style={{ fontSize: "3rem", color: "#198754" }}
            icon={faCircleCheck}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="primary"
            onClick={() => {
              setRegisterSuccessModal(false);
              setEnteredEmail("");
              setEnteredFullName("");
              setEnteredPassword("");
              changeStateHandle();
            }}
          >
            OK!!
          </Button>
        </Modal.Footer>
      </Modal>
      <Form className="" style={{ width: "60%" }} onSubmit={submitHandle}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            name="email"
            onChange={changeEmailHandle}
            value={enteredEmail}
            onBlur={outFocusEmail}
          />
          {showOverplayEmail && (
            <p className={`${styles.overplay}`}>
              <FontAwesomeIcon icon={faCircleExclamation} />
              Có vẻ là email không hợp lệ
            </p>
          )}
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicFullName">
          <Form.Label>Họ và tên</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Full Name"
            name="fullName"
            onChange={changeFullNameHandle}
            value={enteredFullName}
            onBlur={outFocusFullName}
          />
          {showOverplayFullName && (
            <p className={`${styles.overplay}`}>
              <FontAwesomeIcon icon={faCircleExclamation} />
              Họ và tên không hợp lệ
            </p>
          )}
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Mật khẩu</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            name="password"
            onChange={changePasswordHandle}
            value={enteredPassword}
            onBlur={outFocusPassword}
          />
          {showOverplayPassword && (
            <p className={`${styles.overplay}`}>
              <FontAwesomeIcon icon={faCircleExclamation} />
              Mật khẩu cần hơn 8 ký tự
            </p>
          )}
        </Form.Group>
        <div>
          <Button variant="primary" type="submit">
            Đăng ký tài khoản
          </Button>
        </div>
        <p className="mt-3">
          Đã có tài khoản?{" "}
          <strong className="hover" onClick={changeStateHandle}>
            Đăng nhập ngay
          </strong>
        </p>
        <Row>
          <Col className="d-flex align-items-center p-0">
            <div
              style={{ width: "100%", height: "1px", background: "#ddd" }}
            ></div>
          </Col>
          <Col md={"auto"}>tiếp tục với</Col>
          <Col className="d-flex align-items-center p-0">
            <div
              style={{ width: "100%", height: "1px", background: "#ddd" }}
            ></div>
          </Col>
        </Row>
        <div className="w-100 d-flex align-items-center justify-content-center">
          <div
            className="d-flex align-items-center justify-content-center p-2 rounded-circle m-2 hover"
            style={{
              width: "2.5rem",
              height: "2.5rem",
              background: "#ddd",
            }}
            onClick={() => {
              window.location.href =
                process.env.REACT_APP_DOMAIN + "auth/google";
            }}
          >
            <svg
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 48 48"
              className="LgbsSe-Bz112c"
              style={{ width: "100%", height: "100%" }}
            >
              <g>
                <path
                  fill="#EA4335"
                  d="M24 9.5c3.54 0 6.71 1.22 9.21 3.6l6.85-6.85C35.9 2.38 30.47 0 24 0 14.62 0 6.51 5.38 2.56 13.22l7.98 6.19C12.43 13.72 17.74 9.5 24 9.5z"
                ></path>
                <path
                  fill="#4285F4"
                  d="M46.98 24.55c0-1.57-.15-3.09-.38-4.55H24v9.02h12.94c-.58 2.96-2.26 5.48-4.78 7.18l7.73 6c4.51-4.18 7.09-10.36 7.09-17.65z"
                ></path>
                <path
                  fill="#FBBC05"
                  d="M10.53 28.59c-.48-1.45-.76-2.99-.76-4.59s.27-3.14.76-4.59l-7.98-6.19C.92 16.46 0 20.12 0 24c0 3.88.92 7.54 2.56 10.78l7.97-6.19z"
                ></path>
                <path
                  fill="#34A853"
                  d="M24 48c6.48 0 11.93-2.13 15.89-5.81l-7.73-6c-2.15 1.45-4.92 2.3-8.16 2.3-6.26 0-11.57-4.22-13.47-9.91l-7.98 6.19C6.51 42.62 14.62 48 24 48z"
                ></path>
                <path fill="none" d="M0 0h48v48H0z"></path>
              </g>
            </svg>
          </div>
          <div
            className="d-flex align-items-center justify-content-center p-2 rounded-circle m-2 hover"
            style={{
              width: "2.5rem",
              height: "2.5rem",
              background: "#ddd",
              color: "#0088ff",
            }}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 512 512"
              style={{ width: "100%", height: "100%", color: "#0088ff" }}
            >
              <path
                fill="#0088ff"
                d="M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z"
              />
            </svg>
          </div>
        </div>
      </Form>
    </>
  );
}
