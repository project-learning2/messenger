import { faPenToSquare, faUserPen } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import React, { useRef, useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import { useSelector } from "react-redux";

export default function SettingModal({ show, handleClose, handleShow }) {
  const userLogin = useSelector((state) => state.auth.userLogin);
  const [hoverState, setHoverSate] = useState(false);
  const [changeNicknameState, setChangeNicknameState] = useState(false);
  const [img, setImg] = useState("");
  const inputImgRef = useRef(null);
  const [enteredNickname, setEnteredNickname] = useState(null);
  const [file, setFile] = useState(null);

  const changeAvatarHandle = () => {
    inputImgRef.current.click();
  };
  const changeFileHandle = (e) => {
    const file = e.target.files[0];
    setFile(file);
    const reader = new FileReader();
    reader.onload = (e) => {
      setImg(e.target.result);
    };
    reader.readAsDataURL(file);
  };
  const submitHandle = () => {
    const formData = new FormData();
    formData.append("nickname", enteredNickname);
    formData.append("file", file);
    axios
      .post(process.env.REACT_APP_URL_API + "/setting", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then((response) => {
        console.log(response);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Cài đặt</Modal.Title>
        </Modal.Header>
        <Modal.Body className="d-flex flex-column align-items-center">
          <div
            className="hoverr p-2 rounded-circle position-relative"
            onMouseEnter={() => {
              setHoverSate(true);
            }}
            onMouseLeave={() => {
              setHoverSate(false);
            }}
            onClick={changeAvatarHandle}
          >
            {hoverState && (
              <div
                className="position-absolute w-100 h-100 rounded-circle d-flex align-items-center justify-content-center"
                style={{
                  top: "0",
                  right: "0",
                  backgroundColor: "rgba(0,0,0,0.3)",
                }}
              >
                <p className="m-0">
                  <FontAwesomeIcon icon={faPenToSquare} />
                  Chỉnh sửa
                </p>
              </div>
            )}
            {img ? (
              <img
                width={120}
                height={120}
                alt="preview"
                src={img}
                style={{ borderRadius: "50%" }}
              />
            ) : (
              <img
                src={userLogin && userLogin.avatar_img}
                alt="avar"
                width={120}
                height={120}
                style={{ borderRadius: "50%" }}
              />
            )}
            <input
              ref={inputImgRef}
              style={{ display: "none" }}
              accept="image/png, image/jpeg"
              type="file"
              onChange={changeFileHandle}
            />
          </div>
          <h4>{userLogin && userLogin.nickname}</h4>
          <div
            style={{ border: "1px solid #ddd" }}
            className="px-2 py-1 rounded-3 hoverr d-flex align-items-center"
            onClick={() => setChangeNicknameState((b) => !b)}
          >
            <FontAwesomeIcon icon={faUserPen} />
          </div>
          {changeNicknameState && (
            <Form.Group className="my-2" controlId="formBasicEmail">
              {/* <Form.Label>Email</Form.Label> */}
              <Form.Control
                style={{ textAlign: "center" }}
                type="text"
                placeholder="Enter new nickname"
                name="nickname"
                onChange={(e) => {
                  setEnteredNickname(e.target.value);
                }}
                value={enteredNickname}
              />
            </Form.Group>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={submitHandle}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
