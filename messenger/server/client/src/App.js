import "./App.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

import RootLayout from "./components/RootLayout/RootLayout";
import Home from "./pages/Home";
import Chat from "./pages/Chat";
import Testt from "./components/Testt/tesst";
import Auth from "./pages/Auth";
import AuthSucess from "./pages/AuthSucess";
import AuthFail from "./pages/AuthFail";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "/testt",
        element: <Testt />,
      },
    ],
  },
  {
    path: "/messenger/:username",
    element: <Chat />,
  },
  {
    path: "/auth/:token",
    element: <Auth />,
  },
  {
    path: "/test",
    element: <AuthFail />,
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
