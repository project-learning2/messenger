const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const passport = require("passport");
require("./middleware/auth");

// const crypto = require("crypto");
// const token = crypto.randomBytes(32).toString("hex");
// console.log(token);

const app = express();

const store = new MongoDBStore({
  uri: `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.jyxohlo.mongodb.net/${process.env.MONGO_DATABASE}`,
  collection: "sessions",
});
const userOnline = [];
const ChatRoom = require("./model/ChatRoom");
const User = require("./model/User");

const apiRoutes = require("./routes/apiRoutes");

app.use(cors());
app.use(bodyParser.json());
app.use(
  session({
    secret: "my secret",
    resave: false,
    saveUninitialized: false,
    store: store,
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
  if (!req.session.user) {
    return next();
  }
  User.findById(req.session.user._id)
    .then((userResult) => {
      if (!userResult) {
        return next();
      }
      req.user = userResult;
      next();
    })
    .catch((err) => {
      next(new Error(err));
    });
});

// app.use(express.static(pathT));
app.use(express.static(path.join(__dirname, "public")));
app.use("/", express.static(path.join(__dirname, "client", "build")));
app.get(
  "/auth/google",
  passport.authenticate("google", { scope: ["profile", "email"] })
);
app.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    failureRedirect: "http://cayxanh.click/",
  }),
  function (req, res) {
    // Successful authentication, redirect home.
    res.redirect("/");
  }
);
app.use("/api", apiRoutes);
app.use("*", express.static(path.join(__dirname, "client", "build")));

mongoose
  .connect(
    `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.jyxohlo.mongodb.net/${process.env.MONGO_DATABASE}`
  )
  .then(() => {
    const server = app.listen(3000, (err) => {
      if (err) throw err;
      console.log("> Ready on http://localhost:3000");
    });
    const io = require("socket.io")(server, {
      cors: {
        origin: "*",
      },
    });
    io.on("connection", (socket) => {
      console.log("Client connected", socket.id);
      socket.on("user-online", (user) => {
        userOnline.push({
          _id: user._id,
          fullName: user.fullName,
          avatar_img: user.avatar_img,
          socket: socket.id,
        });
        io.emit("newUserOnline", userOnline);
      });
      socket.on("onChat", (data) => {
        const toUser = userOnline.find((el) => el._id === data.toUser);
        console.log(toUser);
        if (toUser) {
          io.to(toUser.socket).emit("messReceived", {
            chatRoom: data.chatRoom,
            contentMess: data.contentMess,
          });
        }
        ChatRoom.findById(data.chatRoom)
          .then((chatRoom) => {
            chatRoom.contentMessage.push(data.contentMess);
            return chatRoom.save();
          })
          .then(() => {
            console.log("add contentMess Success");
          })
          .catch((err) => console.log(err));
      });

      socket.on("disconnect", () => {
        console.log("Client disconnected", socket.id);
        const userIndex = userOnline.findIndex((el) => el.socket === socket.id);
        userOnline.splice(userIndex, 1);
        io.emit("newUserOnline", userOnline);
      });
    });
    console.log("Conected Database by MongoDB");
  });
