const express = require("express");
const router = express.Router();
const multer = require("multer");

const userController = require("../controllers/User");
const authController = require("../controllers/AuthGoogle");

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "public/avatar");
  },
  filename: (req, file, cb) => {
    cb(null, req.user._id + "." + file.originalname.split(".")[1]);
  },
});

router.get("/get-session", userController.getSession);
router.post("/logout", userController.postLogout);
router.post("/create-user", userController.postCreateUser);
router.post("/login", userController.postLogin);
router.post("/create-chat-room", userController.postCreateChatRoom);
router.get("/chat/:user1/:user2", userController.getChat);
router.get("/get-related-chat/:userId", userController.getRelatedChat);
router.post(
  "/setting",
  multer({ storage: fileStorage }).single("file"),
  userController.postpostSetting
);
router.get("/auth-email/:token", userController.getAuthEmail);

router.get("/get-user-gooole", authController.getUserFromGoogle);

module.exports = router;
