const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: { type: String, require: true, unique: true },
  fullName: { type: String, require: true },
  password: { type: String, require: true },
  nickname: { type: String, require: true },
  avatar_img: {
    type: String,
    default: "/avatar/avatar_default.jpg",
  },
  chatRoom: [{ type: Schema.Types.ObjectId, ref: "ChatRoom" }],
});

module.exports = mongoose.model("User", userSchema);
