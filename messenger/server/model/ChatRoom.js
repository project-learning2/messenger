const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const chatRoomSchema = new Schema({
  users: [{ type: Schema.Types.ObjectId, ref: "User" }],
  contentMessage: [
    {
      user: { type: Schema.Types.ObjectId, ref: "User" },
      message: { type: String, require: true },
      date: { type: Date, require: true },
    },
  ],
});

module.exports = mongoose.model("ChatRoom", chatRoomSchema);
