const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const groupMessageSchema = new Schema({
  user: {
    nickname: { type: String, default: null },
    user: { type: Schema.Types.ObjectId, ref: "User" },
  },
  messageContent: { type: String, required: true },
  react: {
    type: String,
    default: "none",
    enum: [
      "none",
      "love",
      "haha",
      "sad",
      "surprise",
      "angry",
      "like",
      "dislike",
    ],
  },
});

module.exports = mongoose.model("GroupMessage", groupMessageSchema);
