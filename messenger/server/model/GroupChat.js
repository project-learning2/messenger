const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const groupChatSchema = new Schema({
  groundName: { type: String, require: true },
  pinnedMessage: { type: Schema.Types.ObjectId, ref: "GroupMessage" },
  message: [
    {
      reply: {
        type: Schema.Types.ObjectId,
        ref: "GroupMessage",
        default: null,
      },
      message: { type: Schema.Types.ObjectId, ref: "GroupMessage" },
    },
  ],
});

module.exports = mongoose.model("GroupChat", groupChatSchema);
