const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const bcryptjs = require("bcryptjs");

const User = require("../model/User");

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.CLIENT_ID_GOOGLE,
      clientSecret: process.env.CLIENT_SECRET,
      callbackURL: "http://cayxanh.click/auth/google/callback",
    },
    function (accessToken, refreshToken, profile, cb) {
      User.findOne({ email: profile._json.email })
        .then((user) => {
          if (user) {
            console.log(user);
            return cb(null, user);
          } else {
            const password = profile._json.sub + "google" + profile._json.email;
            bcryptjs.hash(password, 12).then((hashedPassword) => {
              const newUser = new User({
                email: profile._json.email,
                fullName: profile._json.name,
                password: hashedPassword,
                nickname: profile._json.name,
                avatar_img: profile._json.picture,
              });
              newUser.save().then((result) => {
                return cb(null, result);
              });
            });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  )
);

passport.serializeUser((user, done) => {
  console.log("serializeUser", user);
  return done(null, user);
});

passport.deserializeUser((user, done) => {
  return done(null, user);
});
