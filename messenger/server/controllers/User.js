const User = require("../model/User");
const ChatRoom = require("../model/ChatRoom");
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

exports.getSession = (req, res, next) => {
  if (req.user) {
    res.status(200).json(req.user);
  } else {
    res.status(404).json({ title: "Not found!", message: "No session exits" });
  }
};

exports.postLogout = (req, res, next) => {
  req.session.destroy((err) => {
    console.log(err);
    res.status(200).json({ title: "Success", message: "Logout success!!!" });
  });
};

exports.postCreateUser = (req, res, next) => {
  const fullName = req.body.fullName;
  const email = req.body.email;
  const password = req.body.password;

  const createToken = (userData) => {
    const payload = { userData };
    const token = jwt.sign(payload, "my_secret_key", { expiresIn: "1h" });
    return token;
  };

  User.findOne({ email: email })
    .then((userResult) => {
      if (!userResult) {
        return bcryptjs.hash(password, 12);
      } else {
        res.status(409).json({
          message: "This email is exist!! Pls check again!!",
          title: "Email's exist!",
        });
      }
    })
    .then((hashedPassword) => {
      const newUser = {
        fullName: fullName,
        nickname: fullName,
        email: email,
        password: hashedPassword,
      };
      return newUser;
    })
    .then((newUser) => {
      console.log(newUser);
      const tokenMailAuth = createToken(newUser);
      const msg = {
        to: newUser.email,
        from: "locnguyenddbrvt@gmail.com",
        subject: "Xác thực email của bạn với Mesenger",
        html: `<strong><a href=
          http://localhost:3000/auth/${tokenMailAuth}>Xác thực Email bạn</a></strong>`,
      };
      sgMail.send(msg).then(() => {
        console.log("Email auth sent!!");
        res.end();
      });
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.postLogin = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  User.findOne({ email: email })
    .then((userResult) => {
      if (!userResult) {
        res
          .status(422)
          .json({ title: "Unauthorized", message: "Email's not exist!" });
      } else {
        bcryptjs.compare(password, userResult.password).then((doMatch) => {
          if (doMatch) {
            req.session.isLoggedIn = true;
            req.session.user = userResult;
            req.session.save((err) => {
              if (err) {
                console.log(err);
              } else {
                res.status(200).json({
                  title: "Success!",
                  message: "Login sucess!!",
                  user: userResult,
                });
              }
            });
          } else {
            console.log("herer");
            res.status(422).json({
              title: "Value valid wrong",
              message: "Email or password is wrong, check again!",
            });
          }
        });
      }
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.postCreateChatRoom = (req, res, next) => {
  const users = req.body.users;
  const newChatRoom = new ChatRoom({
    users: users,
    contentMessage: [],
  });
  const promiseArr = [];
  let chatRoom;
  newChatRoom
    .save()
    .then((result) => {
      promiseArr.push(User.findById(users[0]));
      promiseArr.push(User.findById(users[1]));
      chatRoom = result;
      return Promise.all(promiseArr).then((resultPromise) => {
        resultPromise[0].chatRoom.push(result._id);
        resultPromise[1].chatRoom.push(result._id);
        resultPromise[0].save();
        resultPromise[1].save();
      });
    })
    .then(() => {
      res.status(201).json({
        title: "Success!!",
        message: "Create chat room success!!",
        chatRoom: chatRoom,
      });
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.getChat = (req, res, next) => {
  const user1 = req.params.user1;
  const user2 = req.params.user2;

  ChatRoom.findOne({
    users: {
      $size: 2,
      $all: [user1, user2],
    },
  })
    .then((chatroom) => {
      // console.log(chatroom);
      res.status(200).json(chatroom);
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.getRelatedChat = (req, res, next) => {
  const userId = req.params.userId;
  console.log(userId);
  ChatRoom.find({ users: userId })
    .populate("users")
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.postpostSetting = (req, res, next) => {
  const user = req.user;
  const nickname = req.body.nickname;
  const file = req.file;
  // console.log(file.path.split("\\")[2]);
  // console.log(nickname);
  User.findByIdAndUpdate(
    user._id,
    {
      nickname: nickname,
      avatar_img: "/avatar/" + file.path.split("\\")[2],
    },
    { new: true }
  )
    .then((result) => {
      console.log(result);
      req.session.passport.user = result;
      req.session.save((err) => console.log(err));
      console.log(req.originalUrl);
      res.end();
    })
    .catch((err) => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.getAuthEmail = (req, res, next) => {
  const token = req.params.token;

  jwt.verify(token, "my_secret_key", (err, decoded) => {
    if (err) {
      console.log(err);
      res.send("Có lỗi xác thực");
    } else {
      const userData = decoded.userData;
      const newUser = new User(userData);
      newUser
        .save()
        .then((result) => {
          res
            .status(201)
            .json({ title: "Success!", message: "Create User success!!" });
        })
        .catch((err) => {
          console.log("Some thing went wrong here!!");
          const error = new Error(err);
          error.httpStatusCode = 500;
          return next(error);
        });
    }
  });
};
