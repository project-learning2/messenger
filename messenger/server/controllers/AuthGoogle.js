const axios = require("axios");
const querystring = require("querystring");

exports.getUserFromGoogle = (req, res, next) => {
  const code = req.query.code;
  const params = {
    code: code,
    client_id: process.env.CLIENT_ID_GOOGLE,
    client_secret: process.env.CLIENT_SECRET,
    redirect_uri: "http://cayxanh.click/",
    grant_type: "authorization_code",
  };
  const data = querystring.stringify(params);
  const headers = {
    "Content-Type": "application/x-www-form-urlencoded",
  };
  axios
    .post("https://oauth2.googleapis.com/token", data, { headers: headers })
    .then((response) => {
      console.log(response.data);
      res.end();
      // handle the response
    })
    .catch((error) => {
      console.log(error);
      res.end();
      // handle the error
    });
};
